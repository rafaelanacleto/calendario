<?php

/**
 * Cria e Manipula calendario de eventos
 * 
 * PHP version 5.6
 * 
 * @author Rafael Anacleto <rafael.ferreira.anacleto@gmail.com>
 * @copyright (c) 2017, Rafael Anacleto
 * 
 */

class Calendar extends DB_Connect {
    
    /**
     * A data a partir da qual o calendario deve ser criado
     * 
     * Armazendao no formato AAAA-MM-DD HH:MM:SS
     * 
     * @var string A data para usar o calendário
     */
    private $_useDate;
    
    /**
     * O mês para qual o calendario esta sendo criado
     * 
     * @var int O mes do calendario
     */
    private $_m;
    
    /**
     * O ano para qual o calendario esta sendo criado
     * 
     * @var int O ano do calendario
     */
    private $_y;
    
    /**
     * O numero de dias do mes sendo usado
     * 
     * @var int o numero de dia do mes
     */
    private $_daysInMonth;
    
    /**
     * O indice do dia da semana em me começa 0-6
     * 
     * @var int o dia da semana em que o mes começa
     */
    private $_startDay;
    
    /**
     * Cria um objeto de banco de dados e armazena dados relevantes
     * 
     * Na instanciação, esta classe recebe um objeto de banco de dados
     * que, se não for nulo, é armazenado na propriedade privada $_db
     * do objeto. Se for nulo, um novo objeto DBO é criado e armazenado
     * 
     * Informações adcionais são coletadas e armazenadas neste método
     * incluindo o mês a partir do qual o calendário deve ser criado,
     * quantos dias tem no mes, em que dia começa e qual o dia atual.
     * 
     * @param object $dbo um objeto do banco de dados
     * @param string $useDate a data a ser usada para criar o calendario
     * @return void  
     */
    public function __construct($dbo = NULL, $useDate=NULL) {
        parent::__construct($dbo);
        
        //Junta e armazena dados relevantes para o mÊs
        if (isset($useDate)) {
            $this->_useDate = $useDate;
        }else{
            $this->_useDate = date('Y-m-d H:i:s');
        }
        
        //Converte a data pra um timestamp e depois mostra o mes e o ano
        $ts = strtotime($useDate);
        
        $this->_m = date('m', $ts);
        $this->_y = date('Y', $ts);
        
        //Determina quanto dias há no mês
        $this->_daysInMonth = cal_days_in_month(CAL_GREGORIAN, $this->_m, $this->_y);
        
        //Determina em que dia a semana começa
        $ts = mktime(0, 0, 0, $this->_m, 1, $this->_y);
        $this->_startDay = date('w', $ts);       
        
    }
    
    
    
    
        
}
