<?php

/**
 * Ações de banco de dados (Acesso, validação, etc.)
 * 
 * PHP version 5.6
 * 
 * @author Rafael Anacleto <rafael.ferreira.anacleto@gmail.com>
 * @copyright (c) 2017, Rafael Anacleto
 * 
 */

class DB_Connect {
    
    /**
     * Armazena um objeto de banco de dados
     * 
     * @var object Resource Banco MySQL
     */    
    protected $db;
    
    /**
     * Verifica um objeto DB ou cria se não encontrar
     * 
     * @param object $dbo Um objeto banco de dados
     */
    protected function __construct($dbo =NULL) {
        if(is_object($dbo)){
            $this->db = $dbo;
        }else{
            //Constantes serão definidas em /sys/config/db-cred.inc.php
            $dns = "mysql:host=".DB_HOST.";dbname=".DB_NAME;
            
            try {
                $this->db = new PDO($dns, DB_USER, DB_PASS);
            } catch (Exception $exc) {
                ///Se a conexao do banco de dados falhar, imprime o erro
                echo $exc->getTraceAsString();
            }                    
        }
    }
    
        
       
    
    
    
    
}