<?php

//inclue o arquivo de credenciais
include_once '../sys/config/db-cred.inc.php';

foreach ($C as $name => $value) {
    define($name, $value);
}

//cria um objeto PDO
 $dns = "mysql:host=".DB_HOST.";dbname=".DB_NAME;
 $dbo = new PDO($dns, DB_USER, DB_PASS);
 
 //carga automática para as clasess
 
 function __autoload($class) {
     
     $filename = "../sys/class/class.".$class.".inc.php";
     
     if (file_exists($filename)) {
         include_once $filename;         
     }
 }
 