<?php

/*
 * Cria uma matriz vazia para armazenar contantes
 */
$C = array();

/*
 * A URL do hospedeiro do banco de dados
 */
$C['DB_HOST'] = 'localhost';

/*
 * O nome do usario  do banco de dados
 */
$C['DB_USER'] = 'root';

/*
 * A senha do banco de dados
 */
$C['DB_PASS'] = '';

/*
 * O nome do banco de dados schema
 */
$C['DB_NAME'] = 'calendario';